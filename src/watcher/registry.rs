use futures::channel::mpsc;
use futures::FutureExt;
use futures::SinkExt;
use futures::StreamExt;

use crate::trigger::types::TriggerEvent;
use crate::watcher::types::Event;

pub trait Watcher: Send + Sync {
    fn async_start(
        &self,
        report_channel: mpsc::Sender<Event>,
        signal_channel: mpsc::Receiver<TriggerEvent>,
    );
}

pub struct ConcurrencyWatcherRegistry {
    watchers: Vec<Box<dyn Watcher>>,
}
impl ConcurrencyWatcherRegistry {
    pub fn new() -> Self {
        ConcurrencyWatcherRegistry { watchers: vec![] }
    }

    pub fn register(&mut self, watcher: Box<dyn Watcher>) {
        self.watchers.push(watcher)
    }

    pub fn async_run(
        &'static self,
        mut src: mpsc::Receiver<TriggerEvent>,
        dest: mpsc::Sender<Event>,
    ) {
        let mut dispatch_channels = vec![];
        for watcher in &self.watchers {
            let (tx, rx) = mpsc::channel(1024);
            watcher.async_start(dest.clone(), rx);
            dispatch_channels.push((watcher, tx));
        }
        tokio::spawn(
            async move {
                while let Some(msg) = src.next().await {
                    for (watcher, channel) in dispatch_channels.iter_mut() {
                        channel.send(msg.clone()).await.unwrap_or_else(|_| {
                            let (tx, rx) = mpsc::channel(1024);
                            watcher.async_start(dest.clone(), rx);
                            *channel = tx;
                        });
                    }
                }
            }
            .then(|_| async move {
                log::warn!("WatcherRegistry terminate");
            }),
        );
    }
}
