use futures::channel::mpsc::{Receiver, Sender};
use futures::{FutureExt, SinkExt, StreamExt};

use super::*;
use crate::watcher::registry::Watcher;
use crate::watcher::types::Event;

const URLS: &[&str] = &[
    "https://api-v2.chainx.org/home/btcStatus",
    "https://api-v2.chainx.org/trustees",
    "https://api-v2.chainx.org/validators",
    "https://api-v2.chainx.org/unsettled",
    "https://api-v2.chainx.org/extrinsics",
    "https://api-v2.chainx.org/events",
    "https://api-v2.chainx.org/dex/pairs",
    "https://api-v2.chainx.org/crossblocks/bitcoin/trustees",
    "https://api-v2.chainx.org/crossblocks/bitcoin/unclaim",
    "https://api-v2.chainx.org/crossblocks/deposit_mine",
];

pub struct Scan;
impl Watcher for Scan {
    fn async_start(
        &self,
        mut report_channel: Sender<Event>,
        mut signal_channel: Receiver<TriggerEvent>,
    ) {
        tokio::spawn(
            async move {
                let mut schedule_count = 0u128;
                while let Some(event) = signal_channel.next().await {
                    if let TriggerEvent::Schedule = event {
                        if schedule_count % 100 == 0 {
                            let mut count = 0;
                            for &url in URLS {
                                let request_result = reqwest::get(url).await;
                                if let Ok(response) = request_result {
                                    if response.status() == reqwest::StatusCode::OK {
                                        count += 1
                                    } else {
                                        log::debug!(
                                            "Scan response({:?}): {}",
                                            response.status(),
                                            url
                                        );
                                    }
                                }
                                tokio::time::delay_for(tokio::time::Duration::from_secs(60)).await;
                            }
                            log::info!("{} of {} explorer api were available.", count, URLS.len());
                            if count < 5 {
                                report_channel.send(Event::ExplorerDown).await?;
                            }
                        }
                        schedule_count += 1;
                    }
                }
                log::info!("Scan watcher stopped.");
                Ok::<_, anyhow::Error>(())
            }
            .then(|e| async move {
                let _ = crate::diagnostic::get_diagnostic_channel()
                    .send("- Scan watcher stopped!".into())
                    .await;
                if let Err(e) = e {
                    log::warn!("Scan watcher unexpectedly stopped: {:?}", e);
                }
            }),
        );
    }
}
