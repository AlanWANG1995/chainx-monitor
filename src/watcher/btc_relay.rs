use futures::channel::mpsc::{Receiver, Sender};
use futures::{FutureExt, SinkExt, StreamExt};

use super::*;
use crate::config::config;
use crate::utils::build_client;
use crate::watcher::registry::Watcher;
use crate::watcher::types::Event;

async fn get_height_from_explorer() -> anyhow::Result<u64> {
    let mut explorer_response: serde_json::Value =
        reqwest::get("https://api.blockchair.com/bitcoin/stats")
            .await?
            .json()
            .await?;
    let height_in_explorer = explorer_response["data"]["blocks"]
        .take()
        .as_u64()
        .ok_or(anyhow::anyhow!("ParseError"))?;
    Ok(height_in_explorer)
}

pub struct Status;
impl Watcher for Status {
    fn async_start(
        &self,
        mut report_channel: Sender<Event>,
        mut signal_channel: Receiver<TriggerEvent>,
    ) {
        tokio::spawn(
            async move {
                let mut schedule_count = 0u128;
                let mut last_btc_height = 0;
                let client = build_client(config().node_url()).await?;
                while let Some(event) = signal_channel.next().await {
                    if let TriggerEvent::Schedule = event {
                        if schedule_count % 60 == 0 {
                            let head = client.head().await?;
                            let btc_height = client.btc_head_height(Some(head)).await?;
                            let btc_height_in_explorer = {
                                loop {
                                    let result = get_height_from_explorer().await;
                                    if let Ok(h) = result {
                                        break h;
                                    }
                                }
                            };

                            if btc_height_in_explorer as i128 - btc_height as i128 > 6 {
                                report_channel
                                    .send(Event::BTCRelayDown {
                                        last_height: last_btc_height,
                                    })
                                    .await?;
                            } else {
                                log::info!("BTC-relay updated block#{}", btc_height);
                                last_btc_height = btc_height;
                            }
                            schedule_count += 1;
                        }
                    }
                }
                Ok::<_, anyhow::Error>(())
            }
            .then(async move |e| {
                let _ = crate::diagnostic::get_diagnostic_channel()
                    .send("- BTC relay watcher stopped!".into())
                    .await;
                if let Err(e) = e {
                    log::error!("Btc relay watcher unexpectly stopped. Reason: {:?}", e)
                }
            }),
        );
    }
}
