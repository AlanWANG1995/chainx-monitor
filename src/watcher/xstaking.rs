use std::collections::{BTreeMap, VecDeque};

use anyhow::anyhow;
use futures::{
    channel::mpsc::{Receiver, Sender},
    FutureExt, SinkExt, StreamExt,
};
use xcli::runtime::xpallets::xstaking::LockedType;

use super::*;
use crate::config::config;
use crate::types::ValidatorInfo;
use crate::utils::build_client;
use crate::watcher::registry::Watcher;
use crate::watcher::types::Event;

pub struct UnbondedChunk;
impl UnbondedChunk {
    pub async fn run_once(
        event: TriggerEvent,
        client: xcli::runtime::ChainXClient,
        mut report_channel: Sender<Event>,
    ) -> anyhow::Result<()> {
        if let TriggerEvent::Schedule = event {
            let head_hash = client.finalized_head().await?;
            let res = client.get_nominations(Some(head_hash)).await?;
            let vec_of_btreemap = res.iter().map(|(_, v)| v);
            let sum_balance = vec_of_btreemap
                .map(|n| {
                    n.iter()
                        .map(|(_, ledger)| {
                            ledger
                                .unbonded_chunks
                                .iter()
                                .map(|chunk| chunk.value)
                                .sum::<Balance>()
                        })
                        .sum::<Balance>()
                })
                .sum::<Balance>();

            let res = client.xstaking_locks(Some(head_hash)).await?;
            let locked_balance = res
                .iter()
                .map(|(_, balances)| balances.get(&LockedType::BondedWithdrawal).unwrap_or(&0))
                .sum::<Balance>();
            log::info!(
                "Unbounded chunks from accounts is {} where {} in locked.",
                sum_balance,
                locked_balance
            );
            if locked_balance != sum_balance {
                report_channel
                    .send(Event::WrongTotalUnlockingBalances(
                        sum_balance,
                        locked_balance,
                    ))
                    .await?;
            }
        }
        Ok(())
    }
}
impl Watcher for UnbondedChunk {
    fn async_start(
        &self,
        report_channel: Sender<Event>,
        mut signal_channel: Receiver<TriggerEvent>,
    ) {
        tokio::spawn(
            async move {
                let client = build_client(config().node_url()).await?;
                let mut schedule_count = 0u128;
                while let Some(event) = signal_channel.next().await {
                    if schedule_count % (12 * 60) == 0 {
                        Self::run_once(event, client.clone(), report_channel.clone()).await?;
                    }
                    schedule_count += 1;
                }
                Ok::<_, anyhow::Error>(())
            }
            .then(async move |e| {
                if let Err(e) = e {
                    log::error!("Balance watcher unexpectly stopped. Reason: {:?}", e)
                }
            }),
        );
    }
}

pub struct Nomination;
impl Nomination {
    pub async fn run_once(
        event: TriggerEvent,
        client: xcli::runtime::ChainXClient,
        mut report_channel: Sender<Event>,
    ) -> anyhow::Result<()> {
        let head_hash = client.head().await.map_err(|e| anyhow!("{}", e))?;
        let height = client
            .header(Some(head_hash))
            .await
            .map_err(|e| anyhow!("{}", e))?
            .ok_or_else(|| anyhow!("Header with {:?} not found", head_hash))?
            .number as u128;
        let nominations_map = client.get_nominations(Some(head_hash)).await?;

        let mut vote_map = BTreeMap::new();
        for (_, account_nominate) in nominations_map {
            for (k, v) in account_nominate {
                vote_map
                    .entry(k)
                    .and_modify(|(total_nomination, total_vote_weight)| {
                        let nomination = v.nomination;
                        let vote_weight = v.last_vote_weight;
                        let last_update_height = v.last_vote_weight_update as u128;
                        *total_nomination += nomination;
                        *total_vote_weight +=
                            vote_weight + nomination * (height - last_update_height);
                    })
                    .or_insert({
                        let nomination = v.nomination;
                        let vote_weight = v.last_vote_weight;
                        let last_update_height = v.last_vote_weight_update as u128;
                        (
                            nomination,
                            vote_weight + nomination * (height - last_update_height),
                        )
                    });
            }
        }

        let result: Vec<ValidatorInfo> = client.get_validators(Some(head_hash)).await?;
        let mut mismatch_count = 0;
        let mut vote_weight_mismatch_count = 0u32;
        let mut total_node_vote_weight = 0;
        let mut total_account_vote_weight = 0;
        result.iter().enumerate().for_each(|(_, validator)| {
            let total = vote_map.get(&validator.account).map_or((0, 0), |&v| v);
            let validator_nomination = validator
                .total_nomination
                .parse::<u128>()
                .expect("parse total_nomination failed");
            let last_vote_weight = validator
                .last_total_vote_weight
                .parse::<u128>()
                .expect("parse last_total_vote_weight failed");
            let last_update_height = validator.last_total_vote_weight_update as u128;
            let newest_vote_weight =
                last_vote_weight + validator_nomination * (height - last_update_height);
            total_node_vote_weight += newest_vote_weight;
            total_account_vote_weight += total.1;

            if total.0 != validator_nomination {
                log::error!(
                    "{} has {} votes, but actually {} in total.",
                    validator.account,
                    validator_nomination,
                    total.0
                );
                mismatch_count += 1;
            }

            if total.1 != newest_vote_weight {
                log::error!(
                    "{} has {} vote weight, but actually {} in total.",
                    validator.account,
                    newest_vote_weight,
                    total.1
                );
                vote_weight_mismatch_count += 1;
            } else {
                // TODO(wangyafei): if config.verbose_log
                if false {
                    log::debug!(
                        "{} vote weight: {} on the table, {} calculated.",
                        validator.account,
                        newest_vote_weight,
                        total.1
                    );
                }
            }
        });

        log::info!(
            "total vote weight: node {}, account {}",
            total_node_vote_weight,
            total_account_vote_weight
        );
        if mismatch_count > 0 || vote_weight_mismatch_count > 0 {
            report_channel
                .send(Event::WrongTotalNomination(mismatch_count))
                .await?;
        } else {
            log::info!("all {} accounts nomination verified.", result.len());
        }
        Ok(())
    }
}
impl Watcher for Nomination {
    fn async_start(
        &self,
        report_channel: Sender<Event>,
        mut signal_channel: Receiver<TriggerEvent>,
    ) {
        tokio::spawn(
            async move {
                let client = build_client(config().node_url()).await?;
                let mut schedule_count = 0u128;
                while let Some(event) = signal_channel.next().await {
                    if let TriggerEvent::Schedule = event {
                        if schedule_count % (24 * 12) == 30 {
                            Self::run_once(event, client.clone(), report_channel.clone()).await?;
                        }
                        schedule_count += 1;
                    }
                }
                Ok::<_, anyhow::Error>(())
            }
            .then(|e| async move {
                if let Err(e) = e {
                    log::warn!("Watcher unexpectedly stopped: {:?}", e)
                }
            }),
        );
    }
}

pub struct SelfBonded;
impl Watcher for SelfBonded {
    fn async_start(
        &self,
        mut report_channel: Sender<Event>,
        mut signal_channel: Receiver<TriggerEvent>,
    ) {
        type ValidatorsRecord = VecDeque<(u64, Vec<ValidatorInfo>)>;
        tokio::spawn(
            async move {
                let client = build_client(config().node_url()).await?;
                let mut record = ValidatorsRecord::new();
                let mut schedule_count: u128 = 0;
                while let Some(event) = signal_channel.next().await {
                    if let TriggerEvent::Schedule = event {
                        if schedule_count % (24 * 60) == 200 {
                            let result = client.get_validators(None).await?;
                            // FIXME(wangyafei): deserialization may shuffle order
                            if record.len() >= 7 {
                                let seven_day_ago_record =
                                    record.pop_front().ok_or_else(|| anyhow!("empty queue"))?.1;
                                for (a, b) in result.iter().zip(seven_day_ago_record.iter()) {
                                    let now_bonded = a.self_bonded.parse::<u128>()?;
                                    let before_bonded = b.self_bonded.parse::<u128>()?;
                                    if before_bonded > now_bonded
                                        && before_bonded - now_bonded > (before_bonded / 3)
                                    {
                                        report_channel
                                            .send(Event::IrregularSelfBondedUnlock(
                                                (before_bonded - now_bonded) as f64
                                                    / before_bonded as f64,
                                            ))
                                            .await?;
                                    }
                                }
                            } else {
                                record.push_back((crate::utils::now(), result));
                            }
                        }
                        schedule_count += 1;
                    }
                }
                Ok::<_, anyhow::Error>(())
            }
            .then(async move |e| {
                let _ = crate::diagnostic::get_diagnostic_channel()
                    .send("- X-Staking watcher stopped!".into())
                    .await;
                if let Err(e) = e {
                    log::error!("SelfBounded watcher unexpectly stopped. Reason: {:?}", e)
                }
            }),
        );
    }
}
