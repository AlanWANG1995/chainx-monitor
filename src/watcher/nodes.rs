use futures::channel::mpsc::{Receiver, Sender};
use futures::{FutureExt, SinkExt, StreamExt};

use super::registry::Watcher;
use super::types::Event;
use crate::{trigger::types::TriggerEvent, utils::build_client};

const NODES: &[&str] = &[
    "117.51.148.252",
    "117.51.150.77",
    "117.51.142.70",
    "117.51.151.223",
];

const PORT: u16 = 8087;

pub struct Nodes;
impl Watcher for Nodes {
    fn async_start(
        &self,
        mut report_channel: Sender<Event>,
        mut signal_channel: Receiver<TriggerEvent>,
    ) {
        tokio::spawn(
            async move {
                let mut schedule_count = 0u128;
                while let Some(event) = signal_channel.next().await {
                    if let TriggerEvent::Schedule = event {
                        if schedule_count % 10 == 0 {
                            let mut count = 0;
                            for node in NODES {
                                if let Err(_) =
                                    build_client(format!("ws://{}:{}", node, PORT)).await
                                {
                                    count += 1;
                                    log::warn!("Node({}) down.", node);
                                };
                            }
                            if count > 1 {
                                report_channel.send(Event::NodeDown(count)).await?;
                            }
                        }
                        schedule_count += 1;
                    }
                }
                Ok::<_, anyhow::Error>(())
            }
            .then(async move |e| {
                let _ = crate::diagnostic::get_diagnostic_channel()
                    .send("- Node watcher stopped!".into())
                    .await;
                if let Err(e) = e {
                    log::warn!("Node watcher unexpectedly stopped: {:?}", e);
                }
            }),
        );
    }
}
