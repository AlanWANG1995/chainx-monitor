use anyhow::Result;
use futures::channel::mpsc::{Receiver, Sender};
use futures::{FutureExt, SinkExt, StreamExt};

use super::*;
use crate::config::config;
use crate::rpc::ChainXRpc;
use crate::utils::build_client;
use crate::watcher::registry::Watcher;
use crate::watcher::types::Event;

pub struct Balance;
impl Balance {
    pub async fn run_once(client: ChainXClient) -> Result<Option<Event>> {
        let hash = client.finalized_head().await?;
        let mut sum_from_account = 0;
        for (i, (_, value)) in client
            .get_accounts_info(Some(hash))
            .await?
            .iter()
            .enumerate()
        {
            sum_from_account += value.data.free + value.data.reserved;
        }
        let issuance = client.pcx_issuance(Some(hash)).await?;
        log::info!(
            "Current {} PCXs were sum from accounts, where {} recorded by storage",
            sum_from_account,
            issuance
        );
        if issuance != sum_from_account {
            return Ok(Some(Event::WrongTotalIssuance(sum_from_account, issuance)));
        }
        Ok(None)
    }
}
impl Watcher for Balance {
    fn async_start(
        &self,
        mut report_channel: Sender<Event>,
        mut signal_channel: Receiver<TriggerEvent>,
    ) {
        tokio::spawn(
            async move {
                let mut schedule_count = 0u128;
                let client = build_client(config().node_url()).await?;
                while let Some(event) = signal_channel.next().await {
                    if let TriggerEvent::Schedule = event {
                        if schedule_count % (24 * 60) == 0 {
                            if let Some(e) = Self::run_once(client.clone()).await? {
                                report_channel.send(e).await?;
                            }
                        }
                        schedule_count += 1;
                    }
                }
                Ok::<_, anyhow::Error>(())
            }
            .then(async move |e| {
                let _ = crate::diagnostic::get_diagnostic_channel()
                    .send("- Balance watcher stopped!".into())
                    .await;
                if let Err(e) = e {
                    log::error!("Balance watcher unexpectly stopped. Reason: {:?}", e)
                }
            }),
        );
    }
}
