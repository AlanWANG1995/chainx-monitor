use anyhow::{anyhow, Result};
use futures::channel::mpsc::{Receiver, Sender};
use futures::{FutureExt, SinkExt, StreamExt};
use subxt::system::System;
use xcli::runtime::ChainXRuntime;

use super::*;
use crate::config::config;
use crate::utils::build_client;
use crate::watcher::registry::Watcher;
use crate::watcher::types::Event;

pub struct ChainHead;
impl Watcher for ChainHead {
    fn async_start(
        &self,
        mut report_channel: Sender<Event>,
        mut signal_channel: Receiver<TriggerEvent>,
    ) {
        tokio::spawn(
            async move {
                let mut last_head_height = 0;
                while let Some(event) = signal_channel.next().await {
                    if let TriggerEvent::BlockReceive(head) = event {
                        match head {
                            Some(head) => {
                                last_head_height = head.number;
                                log::info!(
                                    "#{}Head block hash: {:?}",
                                    last_head_height,
                                    head.hash(),
                                );
                            }
                            None => {
                                report_channel
                                    .send(Event::NewBlockTakeTooLong(last_head_height))
                                    .await?
                            }
                        };
                    }
                }
                Ok::<_, anyhow::Error>(())
            }
            .then(|err| async move {
                let _ = crate::diagnostic::get_diagnostic_channel()
                    .send("- ChainHead watcher stopped!".into())
                    .await;
                if let Err(e) = err {
                    log::error!("ChainHead watcher unexpectly stopped. Reason: {:?}", e)
                }
            }),
        );
    }
}

pub struct Validator;
impl Watcher for Validator {
    fn async_start(
        &self,
        mut report_channel: Sender<Event>,
        mut signal_channel: Receiver<TriggerEvent>,
    ) {
        tokio::spawn(
            async move {
                while let Some(event) = signal_channel.next().await {
                    if let TriggerEvent::EventReceive(e) = event {
                        if e.variant == "SomeOffline" {
                            log::info!("Some validator offline");
                            report_channel.send(Event::ValidatorOffline).await?;
                        }
                    }
                }
                Ok::<_, anyhow::Error>(())
            }
            .then(|e| async move {
                let _ = crate::diagnostic::get_diagnostic_channel()
                    .send("- Validator watcher stopped!".into())
                    .await;
                if let Err(e) = e {
                    log::error!("Validator watcher unexpectly stopped. Reason: {:?}", e)
                }
            }),
        );
    }
}

pub struct FinalizedHead;
const REFRESH_BLOCK_GAP: u8 = 3;
const NOTIFY_BLOCK_GAP: u8 = 30;
impl Watcher for FinalizedHead {
    fn async_start(
        &self,
        mut report_channel: Sender<Event>,
        mut signal_channel: Receiver<TriggerEvent>,
    ) {
        tokio::spawn(
            async move {
                let client = build_client(config().node_url()).await?;
                let mut last_finalized_height = 0u32;
                while let Some(event) = signal_channel.next().await {
                    if let TriggerEvent::BlockReceive(Some(header)) = event {
                        if header.number > last_finalized_height + REFRESH_BLOCK_GAP as u32 {
                            let hash = client.finalized_head().await?;
                            let finalized_header = client
                                .header(Some(hash))
                                .await?
                                .ok_or_else(|| anyhow!("No match block with {}", hash))?;
                            last_finalized_height = finalized_header.number;

                            let unfinalized_header_height =
                                header.number as i128 - last_finalized_height as i128;

                            log::debug!(
                                "#{}/{}: {} blocks need to be finalized.",
                                header.number,
                                last_finalized_height,
                                unfinalized_header_height
                            );
                            if unfinalized_header_height > NOTIFY_BLOCK_GAP as i128 {
                                report_channel
                                    .send(Event::TooManyUnfinalizedBlocks(
                                        unfinalized_header_height as usize,
                                    ))
                                    .await?;
                            }
                        }
                    }
                }
                Ok::<_, anyhow::Error>(())
            }
            .then(|e| async move {
                let _ = crate::diagnostic::get_diagnostic_channel()
                    .send("- FinalizedHead watcher stopped!".into())
                    .await;
                if let Err(e) = e {
                    log::error!("FinalizedHead watcher unexpectly stopped. Reason: {:?}", e)
                }
            }),
        );
    }
}
