use anyhow::Result;
use futures::channel::mpsc::{Receiver, Sender};
use futures::{FutureExt, SinkExt, StreamExt};

use super::*;
use crate::config::config;
use crate::utils::build_client;
use crate::watcher::registry::Watcher;
use crate::watcher::types::Event;

pub struct XBtc;
impl XBtc {
    pub async fn run_once(
        client: xcli::runtime::ChainXClient,
        mut report_channel: Sender<Event>,
    ) -> Result<()> {
        let hash = client.head().await?;
        let sum_from_account = client
            .btc_asset_balance(Some(hash))
            .await?
            .iter()
            .map(|(_, value)| value)
            .sum::<Balance>();
        let total_asset = client.total_btc_asset(Some(hash)).await?;
        log::info!(
            "Current {} X-BTCs were sum by accounts, where {} recorded by storage",
            sum_from_account,
            total_asset
        );
        if total_asset != sum_from_account {
            report_channel
                .send(Event::WrongTotalBtcIssuance(sum_from_account, total_asset))
                .await?;
        }
        Ok(())
    }
}
impl Watcher for XBtc {
    fn async_start(
        &self,
        report_channel: Sender<Event>,
        mut signal_channel: Receiver<TriggerEvent>,
    ) {
        tokio::spawn(
            async move {
                let client = build_client(config().node_url()).await?;
                let mut schedule_count = 0u128;
                while let Some(event) = signal_channel.next().await {
                    if let TriggerEvent::Schedule = event {
                        if schedule_count % (60 * 6) == 10 {
                            Self::run_once(client.clone(), report_channel.clone()).await?;
                        }
                        schedule_count += 1;
                    }
                }
                Ok::<_, anyhow::Error>(())
            }
            .then(async move |e| {
                let _ = crate::diagnostic::get_diagnostic_channel()
                    .send("- X-BTC watcher stopped!".into())
                    .await;
                if let Err(e) = e {
                    log::error!("XBtc watcher unexpectly stopped. Reason: {:?}", e)
                }
            }),
        );
    }
}
