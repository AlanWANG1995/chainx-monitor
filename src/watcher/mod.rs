mod balances;
mod btc_relay;
mod chain;
mod nodes;
mod registry;
mod scan;
pub mod types;
mod xassets;
mod xstaking;

use once_cell::sync::OnceCell;
use xcli::runtime::{primitives::Balance, ChainXClient};

use crate::monitor::Monitor;
use crate::rpc::ChainXRpc;
use crate::trigger::types::TriggerEvent;
use crate::watcher::registry::ConcurrencyWatcherRegistry;

pub type Middleware = Box<dyn Fn(ChainXClient, Monitor, TriggerEvent) + Send + Sync + 'static>;

pub fn watcher_registry() -> &'static ConcurrencyWatcherRegistry {
    static REGISTRY: OnceCell<ConcurrencyWatcherRegistry> = OnceCell::new();
    REGISTRY.get_or_init(|| {
        let mut registry = ConcurrencyWatcherRegistry::new();
        registry.register(Box::new(scan::Scan));
        registry.register(Box::new(balances::Balance));
        registry.register(Box::new(chain::ChainHead));
        registry.register(Box::new(chain::Validator));
        registry.register(Box::new(chain::FinalizedHead));
        registry.register(Box::new(btc_relay::Status));
        registry.register(Box::new(xassets::XBtc));
        registry.register(Box::new(xstaking::Nomination));
        registry.register(Box::new(xstaking::UnbondedChunk));
        registry.register(Box::new(xstaking::SelfBonded));
        registry.register(Box::new(nodes::Nodes));
        registry
    })
}
