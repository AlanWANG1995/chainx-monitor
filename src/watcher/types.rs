use enum_kinds::EnumKind;
use serde::{Deserialize, Serialize};
use strum::{EnumIter, IntoEnumIterator};
use xcli::runtime::primitives::BlockNumber;

#[derive(Debug, Clone, Copy, EnumKind)]
#[enum_kind(EventKind, derive(PartialOrd, Ord, Serialize, Deserialize, EnumIter))]
pub enum Event {
    NodeDown(usize),
    ValidatorOffline,
    ExplorerDown,
    BTCRelayDown { last_height: u32 },
    NewBlockTakeTooLong(BlockNumber),
    TooManyUnfinalizedBlocks(usize),
    IrregularSelfBondedUnlock(f64),
    WrongTotalIssuance(u128, u128),
    WrongTotalBtcIssuance(u128, u128),
    WrongTotalNomination(u32),
    WrongTotalUnlockingBalances(u128, u128),
}
