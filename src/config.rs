use std::collections::BTreeSet;
use std::sync::{Arc, RwLock};

use once_cell::sync::OnceCell;
use structopt::StructOpt;

use crate::watcher::types::EventKind;

#[derive(Debug, StructOpt, Clone)]
pub struct MonitorOption {
    #[structopt(long, default_value = "ws://127.0.0.1:8087")]
    node_url: String,
    #[structopt(long, default_value = "5030")]
    rpc_port: u16,
    #[structopt(long, default_value = "2000")]
    timeout: u32,
    #[structopt(long)]
    debug: bool,
}

impl Config {
    pub fn debug(&self) -> bool {
        self.option.debug
    }

    pub fn node_url(&self) -> &str {
        &self.option.node_url
    }

    pub fn rpc_port(&self) -> u16 {
        self.option.rpc_port
    }

    pub fn enable_event(&self, event_kind: EventKind, state: bool) {
        if !state {
            if let Ok(mut set) = self.disabled_event.write() {
                set.insert(event_kind);
            }
        } else if let Ok(mut set) = self.disabled_event.write() {
            set.remove(&event_kind);
        }
    }

    pub fn check_event_enabled(&self, event_kind: EventKind) -> bool {
        if let Ok(set) = self.disabled_event.read() {
            !set.contains(&event_kind)
        } else {
            false
        }
    }
}

pub struct Config {
    option: MonitorOption,
    disabled_event: Arc<RwLock<BTreeSet<EventKind>>>,
}

pub fn config() -> &'static Config {
    static CONFIG: OnceCell<Config> = OnceCell::new();
    CONFIG.get_or_init(|| Config {
        option: MonitorOption::from_args(),
        disabled_event: Arc::new(RwLock::new(BTreeSet::default())),
    })
}
