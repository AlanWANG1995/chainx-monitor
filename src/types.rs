use codec::{Decode, Encode};
use serde::{Deserialize, Serialize};
use serde_json::Value as JsonValue;
use sp_finality_grandpa::AuthorityId;
use sp_runtime::RuntimeDebug;
use std::collections::BTreeMap;
use std::collections::BTreeSet;
use xcli::runtime::primitives::*;

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct ApiValidators {
    pub newitems: Vec<JsonValue>,
    pub page: u64,
    pub page_size: u64,
    pub total: u64,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Prevotes {
    pub current_weight: u32,
    pub missing: BTreeSet<AuthorityId>,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Precommits {
    pub current_weight: u32,
    pub missing: BTreeSet<AuthorityId>,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct RoundState {
    pub round: u32,
    pub total_weight: u32,
    pub threshold_weight: u32,
    pub prevotes: Prevotes,
    pub precommits: Precommits,
}

/// The state of the current best round, as well as the background rounds in a
/// form suitable for serialization.
#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct ReportedRoundStates {
    pub set_id: u32,
    pub best: RoundState,
    pub background: Vec<RoundState>,
}

/// Total information about a validator.
#[derive(PartialEq, Eq, Clone, Serialize, Deserialize, RuntimeDebug)]
#[serde(rename_all = "camelCase")]
pub struct ValidatorInfo {
    pub account: AccountId,
    pub registered_at: BlockNumber,
    pub is_chilled: bool,
    pub last_chilled: Option<BlockNumber>,
    pub referral_id: String,
    pub total_nomination: String,
    pub last_total_vote_weight: String,
    pub last_total_vote_weight_update: BlockNumber,
    pub is_validating: bool,
    pub self_bonded: String,
    pub reward_pot_account: AccountId,
    pub reward_pot_balance: String,
}

/// Type for noting when the unbonded fund can be withdrawn.
#[derive(PartialEq, Eq, Clone, Serialize, Deserialize, RuntimeDebug, Encode, Decode)]
#[serde(rename_all = "camelCase")]
pub struct Unbonded<Balance, BlockNumber> {
    /// Amount of funds to be unlocked.
    pub value: Balance,
    /// Block number at which point it'll be unlocked.
    pub locked_until: BlockNumber,
}

#[derive(PartialEq, Eq, Clone, Serialize, Deserialize, RuntimeDebug, Encode, Decode)]
#[serde(rename_all = "camelCase")]
pub struct BtcHeaderIndex {
    pub header: Hash,
    pub height: u32,
}

/// Vote weight properties of nominator.
#[derive(PartialEq, Eq, Clone, Default, Serialize, Deserialize, RuntimeDebug, Encode, Decode)]
#[serde(rename_all = "camelCase")]
pub struct NominatorLedger<Balance, VoteWeight, BlockNumber> {
    /// The amount of vote.
    pub nomination: Balance,
    /// Last calculated total vote weight of current nominator.
    pub last_vote_weight: VoteWeight,
    /// Block number at which point `last_vote_weight` just updated.
    pub last_vote_weight_update: BlockNumber,
    /// Unbonded entries.
    pub unbonded_chunks: Vec<Unbonded<Balance, BlockNumber>>,
}

pub type NominationMap = BTreeMap<AccountId, NominatorLedger<Balance, u128, BlockNumber>>;
