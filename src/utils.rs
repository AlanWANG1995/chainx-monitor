use anyhow::Result;
use subxt::ClientBuilder;
use xcli::runtime::{ChainXClient, ChainXRuntime};

/// Builds a ChainX runtime specific client.
pub async fn build_client<U: Into<String>>(url: U) -> Result<ChainXClient> {
    Ok(ClientBuilder::<ChainXRuntime>::new()
        .set_url(url)
        .build()
        .await?)
}

pub fn now() -> u64 {
    std::time::UNIX_EPOCH.elapsed().unwrap().as_secs() as u64
}
