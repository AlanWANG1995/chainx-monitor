use futures::channel::mpsc::{channel, Sender};
use futures::{FutureExt, SinkExt, StreamExt};
use once_cell::sync::OnceCell;

const DING_TALK_URL: &str = "https://oapi.dingtalk.com/robot/send?access_token=656702388043e9dfa7d4abff4e3f128d85a62cb7f38eb1cad314a37cdef0ab76";

pub fn get_diagnostic_channel() -> Sender<String> {
    static SENDER: OnceCell<Sender<String>> = OnceCell::new();
    let tx = SENDER.get_or_init(|| {
        let (tx, mut rx) = channel(2048);
        let client = reqwest::Client::default();
        tokio::spawn(async move {
            let mut msg_received = rx.next();
            let mut interval = tokio::time::interval(tokio::time::Duration::from_secs(1200));
            let mut msgs = vec![];
            loop {
                futures::select_biased! {
                   msg  = msg_received => {
                       if let Some(msg) = msg {
                           msgs.push(msg);
                       }
                   },
                   _ = interval.tick().fuse() => {
                        client.post(DING_TALK_URL).json(&serde_json::json!({
                            "msgtype": "markdown",
                            "markdown": {
                                "title": "Digest",
                                "text": msgs.join("\n"),
                            }
                        })).send().await.unwrap();
                        msgs.clear();
                   }
                }
            }
        });
        tx
    });
    tx.clone()
}

#[tokio::test]
async fn send_msg() {
    let mut sender = get_diagnostic_channel();
    sender.send("- 1".into()).await.unwrap();
    sender.send("- 2".into()).await.unwrap();
    tokio::time::delay_for(tokio::time::Duration::from_secs(5)).await;
    sender.send("- 3".into()).await.unwrap();
    tokio::time::delay_for(tokio::time::Duration::from_secs(5)).await;
}
