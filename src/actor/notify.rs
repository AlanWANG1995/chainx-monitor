use crate::actor::registry::{Control, SequenceActor};
use crate::monitor::service::{async_notify, async_send_block_timeout};
use crate::watcher::types::Event;

pub struct Notify;

impl SequenceActor for Notify {
    fn act(&self, event: Event) -> Control {
        match event {
            Event::NodeDown(node) => {
                async_notify(format!("{}节点连接超时", node));
                Control::Continue
            }
            Event::ValidatorOffline => {
                async_notify("验证节点掉线".into());
                Control::Continue
            }
            Event::TooManyUnfinalizedBlocks(num) => {
                async_notify(format!("超过{}个区块未确认", num));
                Control::Continue
            }
            Event::WrongTotalUnlockingBalances(chunks_sum, locked) => {
                async_notify(format!("待解锁金额不符 {}/{}", chunks_sum, locked));
                Control::Continue
            }
            Event::NewBlockTakeTooLong(height) => {
                async_send_block_timeout(height);
                Control::Continue
            }
            Event::IrregularSelfBondedUnlock(percent) => {
                async_notify(format!("一周内自抵押解锁超过{:.2}", percent));
                Control::Continue
            }
            Event::WrongTotalIssuance(sum, issuance) => {
                async_notify(format!("PCX总量错误{}/{}", sum, issuance));
                Control::Continue
            }
            Event::WrongTotalBtcIssuance(sum, issuance) => {
                async_notify(format!("BTC总量错误{}/{}", sum, issuance));
                Control::Continue
            }
            Event::ExplorerDown => {
                async_notify("浏览器不可访问.".into());
                Control::Continue
            }
            Event::BTCRelayDown { .. } => {
                async_notify("Relay状态异常".into());
                Control::Continue
            }
            Event::WrongTotalNomination(..) => todo!(),
        }
    }
}
