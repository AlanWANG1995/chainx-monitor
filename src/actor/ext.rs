use crate::actor::registry::{Control, SequenceActor};
use crate::config::config;
use crate::watcher::types::{Event, EventKind};

pub struct Log;
impl SequenceActor for Log {
    fn act(&self, event: Event) -> Control {
        log::info!("{:?} handled.", EventKind::from(event));
        Control::Continue
    }
}

pub struct DebugBreaker;
impl SequenceActor for DebugBreaker {
    fn act(&self, _: Event) -> Control {
        if config().debug() {
            return Control::Break;
        }
        Control::Continue
    }
}

pub struct EventStatus;
impl SequenceActor for EventStatus {
    fn act(&self, event: Event) -> Control {
        let kind = EventKind::from(&event);
        if config().check_event_enabled(kind) {
            Control::Continue
        } else {
            Control::Break
        }
    }
}
