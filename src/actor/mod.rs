use once_cell::sync::OnceCell;

use crate::actor::registry::SequenceActorRegistry;

mod ext;
mod notify;
mod registry;

pub fn sequence_actor_registry() -> &'static SequenceActorRegistry {
    static SEQUENCE_ACTOR_REGISTRY: OnceCell<SequenceActorRegistry> = OnceCell::new();
    SEQUENCE_ACTOR_REGISTRY.get_or_init(|| {
        let mut instance = SequenceActorRegistry::new();
        instance.register(Box::new(ext::EventStatus));
        instance.register(Box::new(ext::Log));
        instance.register(Box::new(ext::DebugBreaker));
        instance.register(Box::new(notify::Notify));
        instance
    })
}
