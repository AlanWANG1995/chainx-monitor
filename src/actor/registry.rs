use futures::channel::mpsc::Receiver;
use futures::StreamExt;

use crate::watcher::types::Event;

pub enum Control {
    Continue,
    Break,
}

pub trait SequenceActor: Send + Sync {
    fn act(&self, event: Event) -> Control;
}

pub struct SequenceActorRegistry {
    actors: Vec<Box<dyn SequenceActor>>,
}

impl SequenceActorRegistry {
    pub fn new() -> Self {
        Self { actors: vec![] }
    }

    pub fn register(&mut self, actor: Box<dyn SequenceActor>) {
        self.actors.push(actor)
    }

    pub fn async_run(&'static self, mut signal: Receiver<Event>) {
        tokio::spawn(async move {
            while let Some(e) = signal.next().await {
                for actor in &self.actors {
                    if let Control::Break = actor.act(e) {
                        break;
                    }
                }
            }
        });
    }
}
