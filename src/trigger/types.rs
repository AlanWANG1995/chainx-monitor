use subxt::system::System;
use xcli::runtime::ChainXRuntime;

#[derive(Clone, Debug)]
pub enum TriggerEvent {
    BlockReceive(Option<<ChainXRuntime as System>::Header>),
    EventReceive(subxt::RawEvent),
    Schedule,
}
