use anyhow::Result;
use futures::channel::mpsc::Sender;
use futures::SinkExt;
use tokio::time::Duration;

use super::registry::Trigger;
use crate::diagnostic::get_diagnostic_channel;
use crate::trigger::types::TriggerEvent;

pub struct TimerTrigger;

impl Trigger for TimerTrigger {
    fn async_start(&self, mut tx: Sender<TriggerEvent>) -> Result<()> {
        tokio::spawn(async move {
            loop {
                tx.send(TriggerEvent::Schedule)
                    .await
                    .unwrap_or_else(|err| log::error!("TimerTrigger {:?}", err));
                tokio::time::delay_for(Duration::from_secs(60)).await;
                log::debug!("TimerTrigger online.");
            }
            let mut diagnostic = get_diagnostic_channel();
            diagnostic
                .send("TimerTirigger Broken".to_owned())
                .await
                .expect("Diagnostic channle not working");
        });
        Ok(())
    }
}
