mod chain;
pub mod registry;
mod timer;
pub mod types;

use once_cell::sync::OnceCell;
use registry::ConcurrencyTriggerRegistry;

pub fn trigger_registry() -> &'static ConcurrencyTriggerRegistry {
    static TRIGGER_REGISTRY: OnceCell<ConcurrencyTriggerRegistry> = OnceCell::new();
    TRIGGER_REGISTRY.get_or_init(|| {
        let mut instance = ConcurrencyTriggerRegistry::new();
        instance
            .register(Box::new(timer::TimerTrigger))
            .expect("Register TimerTrigger failed.");
        instance
            .register(Box::new(chain::EventTrigger))
            .expect("Register EventTrigger failed.");
        instance
            .register(Box::new(chain::BlockTrigger))
            .expect("Register BlockTrigger failed.");
        instance
    })
}
