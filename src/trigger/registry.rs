use std::clone::Clone;

use anyhow::Result;
use futures::channel::mpsc::Sender;

use crate::trigger::types::TriggerEvent;

pub trait Trigger: Send + Sync + 'static {
    fn async_start(&self, sx: Sender<TriggerEvent>) -> Result<()>;
}

pub struct ConcurrencyTriggerRegistry {
    triggers: Vec<Box<dyn Trigger>>,
}

impl ConcurrencyTriggerRegistry {
    pub fn new() -> Self {
        ConcurrencyTriggerRegistry { triggers: vec![] }
    }
    pub fn register(&mut self, trigger: Box<dyn Trigger>) -> Result<()> {
        self.triggers.push(trigger);
        Ok(())
    }

    pub fn run(&self, tx: Sender<TriggerEvent>) -> Result<()> {
        for trigger in &self.triggers {
            trigger.async_start(tx.clone())?;
        }
        Ok(())
    }
}
