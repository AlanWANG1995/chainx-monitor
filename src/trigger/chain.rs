use anyhow::Result;
use futures::channel::mpsc::Sender;
use futures::SinkExt;
use subxt::{EventSubscription, EventsDecoder};
use tokio::time::{delay_for, timeout, Duration};
use xcli::runtime::ChainXRuntime;

use crate::config::config;
use crate::diagnostic::get_diagnostic_channel;
use crate::trigger::registry::Trigger;
use crate::trigger::types::TriggerEvent;
use crate::utils::build_client;

pub struct BlockTrigger;
impl Trigger for BlockTrigger {
    fn async_start(&self, mut tx: Sender<TriggerEvent>) -> Result<()> {
        let url = config().node_url();
        // TODO(wangyafei): use `Config`
        tokio::spawn(async move {
            loop {
                if let Ok(client) = build_client(url).await {
                    if let Ok(mut sub) = client.subscribe_blocks().await {
                        loop {
                            match timeout(Duration::from_secs(60), sub.next()).await {
                                Ok(None) => break,
                                Ok(Some(head)) => tx
                                    .send(TriggerEvent::BlockReceive(Some(head)))
                                    .await
                                    .unwrap_or_else(|err| {
                                        log::error!("BlockTrigger send failed: {:?}", err)
                                    }),
                                Err(_) => tx
                                    .send(TriggerEvent::BlockReceive(None))
                                    .await
                                    .unwrap_or_else(|err| {
                                        log::error!("BlockTrigger send failed: {:?}", err)
                                    }),
                            };
                        }
                        log::debug!("BlockTrigger broken.(Due to subscribe connection broken)");
                    }
                }
                let mut sender = get_diagnostic_channel();
                sender
                    .send("`BlockTrigger` broken".to_owned())
                    .await
                    .expect("Diagnostic channel not working.");
                delay_for(Duration::from_secs(3)).await;
            }
        });
        Ok(())
    }
}

pub struct EventTrigger;
impl Trigger for EventTrigger {
    fn async_start(&self, mut tx: Sender<TriggerEvent>) -> Result<()> {
        let url = config().node_url();
        tokio::spawn(async move {
            loop {
                if let Ok(client) = build_client(url).await {
                    if let Ok(sub) = client.subscribe_events().await {
                        let decoder =
                            EventsDecoder::<ChainXRuntime>::new(client.metadata().clone());
                        let mut sub = EventSubscription::<ChainXRuntime>::new(sub, decoder);
                        #[allow(irrefutable_let_patterns)]
                        while let event = sub.next().await {
                            if let Some(Ok(raw)) = event {
                                tx.send(TriggerEvent::EventReceive(raw))
                                    .await
                                    .unwrap_or_else(|err| {
                                        log::error!("EventTrigger send event failed: {:?}", err)
                                    });
                            }
                        }
                        log::debug!("EventTrigger broken due to subscription broken.");
                    }
                }
                let mut sender = get_diagnostic_channel();
                sender
                    .send("`EventTrigger` broken".to_owned())
                    .await
                    .expect("Diagnostic channel not working");
                std::thread::sleep(std::time::Duration::from_secs(3));
            }
        });
        Ok(())
    }
}
