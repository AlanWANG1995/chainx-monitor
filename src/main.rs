#![feature(async_closure, map_first_last)]

mod actor;
mod config;
mod diagnostic;
mod monitor;
mod rpc;
mod trigger;
mod types;
mod utils;
mod watcher;

pub fn run() {
    log4rs::init_file("log4rs.yml", Default::default())
        .expect("No log4rs configuration file found.");

    let monitor = monitor::Monitor::new();

    monitor.run();
}

#[tokio::main]
async fn main() {
    run();
    rpc::server::serve().await;
}
