use std::collections::BTreeMap;
use std::str::FromStr;

use anyhow::{anyhow, Result};
use async_trait::async_trait;
use codec::Decode;
use serde_json::to_value;
use sp_core::{
    storage::{StorageData, StorageKey},
    twox_128,
};

use subxt::{system::AccountInfo, Params};
use xcli::runtime::{
    primitives::{AccountId, AssetId, Balance, BlockNumber, Hash},
    xpallets::{xassets::AssetType, xstaking::LockedType},
    ChainXClient, ChainXRuntime,
};

use crate::types::{BtcHeaderIndex, NominationMap, NominatorLedger, ValidatorInfo};

const TWOX_HASH_LEN: usize = 16; // 8 bytes hex
const STORAGE_PREFIX_LEN: usize = 64; // 32 bytes hex
const BLAKE_HASH_LEN: usize = 32; // 16 bytes hex

fn storage_prefix_for(module: &str, storage_name: &str) -> Vec<u8> {
    let mut storage_prefix = twox_128(module.as_bytes()).to_vec();
    storage_prefix.extend_from_slice(&twox_128(storage_name.as_bytes()));
    storage_prefix
}

struct StorageKeyParser {
    data: String,
}

impl StorageKeyParser {
    pub fn new(key: &StorageKey) -> Self {
        StorageKeyParser {
            data: hex::encode(&key.0)[STORAGE_PREFIX_LEN..].to_string(),
        }
    }

    pub fn consume<T>(&mut self, hash_len: usize, data_len: usize) -> Result<T>
    where
        T: Sized + FromStr,
        T::Err: std::fmt::Display,
    {
        let hkk = &self.data[..hash_len + data_len];
        let k = &hkk[hash_len..];
        let result = k.parse::<T>().map_err(|e| anyhow!("{}", e))?;
        self.data = self.data[hash_len + data_len..].to_string();
        Ok(result)
    }

    pub fn consume_raw_bytes(&mut self, hash_len: usize, data_len: usize) -> Result<AssetId> {
        let hkk = &self.data[..hash_len + data_len];
        let k = &hkk[hash_len..];
        let result = {
            let mut asset_id = [0u8; 4];
            asset_id.copy_from_slice(hex::decode(k)?.as_slice());
            Ok(AssetId::from_le_bytes(asset_id))
        };
        self.data = self.data[hash_len + data_len..].to_string();
        result
    }
}

#[async_trait]
pub trait ChainXRpc {
    async fn get_validators(&self, hash: Option<Hash>) -> Result<Vec<ValidatorInfo>>;
    async fn get_nominations(
        &self,
        hash: Option<Hash>,
    ) -> Result<BTreeMap<AccountId, NominationMap>>;
    async fn get_accounts_info(
        &self,
        hash: Option<Hash>,
    ) -> Result<BTreeMap<AccountId, AccountInfo<ChainXRuntime>>>;
    async fn pairs(
        &self,
        prefix: Vec<u8>,
        hash: Option<Hash>,
    ) -> Result<Vec<(StorageKey, StorageData)>>;

    async fn pcx_issuance(&self, hash: Option<Hash>) -> Result<Balance>;

    async fn btc_asset_balance(&self, hash: Option<Hash>) -> Result<BTreeMap<AccountId, Balance>>;
    async fn total_btc_asset(&self, hash: Option<Hash>) -> Result<Balance>;

    async fn xstaking_locks(
        &self,
        hash: Option<Hash>,
    ) -> Result<BTreeMap<AccountId, BTreeMap<LockedType, Balance>>>;

    async fn storage(&self, key: Vec<u8>, hash: Option<Hash>) -> Result<Option<StorageData>>;
    async fn btc_head_height(&self, hash: Option<Hash>) -> Result<u32>;
}

#[async_trait]
impl ChainXRpc for ChainXClient {
    async fn get_validators(&self, hash: Option<Hash>) -> Result<Vec<ValidatorInfo>> {
        let params = Params::Array(vec![to_value(hash)?]);
        let validators = self.request("xstaking_getValidators", params).await?;
        Ok(validators)
    }

    async fn get_nominations(
        &self,
        hash: Option<Hash>,
    ) -> Result<BTreeMap<AccountId, NominationMap>> {
        let prefix = storage_prefix_for("XStaking", "Nominations");
        let data = self.pairs(prefix, hash).await?;
        let mut nominations = BTreeMap::<AccountId, NominationMap>::new();
        for (key, value) in data {
            let mut key_parser = StorageKeyParser::new(&key);
            let nominator = key_parser.consume(TWOX_HASH_LEN, 64)?;
            let nominee = key_parser.consume(TWOX_HASH_LEN, 64)?;
            let nominator_ledger: NominatorLedger<u128, u128, BlockNumber> =
                Decode::decode(&mut value.0.as_slice())?;

            let entry = nominations.entry(nominator).or_default();
            entry.insert(nominee, nominator_ledger);
        }
        Ok(nominations)
    }

    async fn get_accounts_info(
        &self,
        hash: Option<Hash>,
    ) -> Result<BTreeMap<AccountId, AccountInfo<ChainXRuntime>>> {
        let prefix = storage_prefix_for("System", "Account");
        let data = self.pairs(prefix, hash).await?;
        let mut result = BTreeMap::new();
        for (key, value) in data {
            let mut key_parser = StorageKeyParser::new(&key);
            let account_id = key_parser.consume(BLAKE_HASH_LEN, 64)?;
            let account_info: AccountInfo<ChainXRuntime> = Decode::decode(&mut value.0.as_slice())?;

            result.insert(account_id, account_info);
        }
        Ok(result)
    }

    async fn pairs(
        &self,
        prefix: Vec<u8>,
        hash: Option<Hash>,
    ) -> Result<Vec<(StorageKey, StorageData)>> {
        let params = match hash {
            Some(hash) => Params::Array(vec![to_value(StorageKey(prefix))?, to_value(hash)?]),
            None => Params::Array(vec![to_value(StorageKey(prefix))?]),
        };
        Ok(self.request("state_getPairs", params).await?)
    }

    async fn pcx_issuance(&self, hash: Option<Hash>) -> Result<Balance> {
        let prefix = storage_prefix_for("Balances", "TotalIssuance");
        let data = self.pairs(prefix, hash).await?;
        let (_, value) = data
            .get(0)
            .ok_or_else(|| anyhow!("Balances/TotalIssuance down"))?;
        let b: Balance = Decode::decode(&mut value.0.as_slice())?;
        Ok(b)
    }
    async fn btc_asset_balance(&self, hash: Option<Hash>) -> Result<BTreeMap<AccountId, Balance>> {
        let prefix = storage_prefix_for("XAssets", "AssetBalance");
        let data = self.pairs(prefix, hash).await?;
        let mut assets =
            BTreeMap::<AccountId, BTreeMap<AssetId, BTreeMap<AssetType, Balance>>>::new();
        for (key, value) in data {
            let mut key_parser = StorageKeyParser::new(&key);
            let account = key_parser.consume(BLAKE_HASH_LEN, 64)?;
            let asset_id = key_parser.consume_raw_bytes(TWOX_HASH_LEN, 8)?;
            let asset_balance: BTreeMap<AssetType, Balance> =
                Decode::decode(&mut value.0.as_slice())?;

            let entry = assets.entry(account).or_default();
            entry.insert(asset_id, asset_balance);
        }
        let result = assets
            .iter()
            .map(|(k, v)| {
                (
                    k.clone(),
                    *v.get(&1).unwrap().get(&AssetType::Usable).unwrap_or(&0),
                )
            })
            .collect();
        Ok(result)
    }

    async fn total_btc_asset(&self, hash: Option<Hash>) -> Result<Balance> {
        let prefix = storage_prefix_for("XAssets", "TotalAssetBalance");
        let data = self.pairs(prefix, hash).await?;
        let mut total_asset_balance = BTreeMap::new();
        for (key, value) in data {
            let mut key_parse = StorageKeyParser::new(&key);
            let asset_id = key_parse.consume_raw_bytes(TWOX_HASH_LEN, 8)?;

            let asset_balance: BTreeMap<AssetType, Balance> =
                Decode::decode(&mut value.0.as_slice())?;

            total_asset_balance.insert(asset_id, asset_balance);
        }
        Ok(*total_asset_balance
            .get(&1)
            .unwrap()
            .get(&AssetType::Usable)
            .unwrap())
    }

    async fn xstaking_locks(
        &self,
        hash: Option<Hash>,
    ) -> Result<BTreeMap<AccountId, BTreeMap<LockedType, Balance>>> {
        let prefix = storage_prefix_for("XStaking", "Locks");
        let data = self.pairs(prefix, hash).await?;
        let mut result = BTreeMap::new();
        for (key, value) in data {
            let mut key_parser = StorageKeyParser::new(&key);
            let account_id = key_parser.consume(BLAKE_HASH_LEN, 64)?;
            let asset_balance: BTreeMap<LockedType, Balance> =
                Decode::decode(&mut value.0.as_slice())?;
            result.insert(account_id, asset_balance);
        }
        Ok(result)
    }

    async fn storage(&self, key: Vec<u8>, hash: Option<Hash>) -> Result<Option<StorageData>> {
        let params = Params::Array(vec![to_value(StorageKey(key))?, to_value(hash)?]);
        let data = self.request("state_getStorage", params).await?;
        Ok(data)
    }

    async fn btc_head_height(&self, hash: Option<Hash>) -> Result<u32> {
        let prefix = storage_prefix_for("XGatewayBitcoin", "BestIndex");
        let data = self
            .storage(prefix, hash)
            .await?
            .ok_or_else(|| anyhow!("No data with such key XGatewayBitcoin/BestIndex"))?;
        let btc_index: BtcHeaderIndex = Decode::decode(&mut data.0.as_slice())?;
        Ok(btc_index.height)
    }
}
