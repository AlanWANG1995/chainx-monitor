use anyhow::Result;
use serde_json::json;
use warp::Filter;

use crate::config::config;
use crate::rpc::types::ApiBody;

pub async fn serve() {
    let promote = warp::post()
        .and(warp::path("api"))
        .and(warp::path("v1"))
        .and(warp::body::content_length_limit(1024 * 16))
        .and(warp::body::json())
        .and_then(|body: ApiBody| async move {
            match body {
                ApiBody::RpcSendSMSRequest {
                    phone,
                    params,
                    template_id,
                } => {
                    log::info!("Receive rpc request");
                    let aliclient = crate::monitor::service::AliServiceClient::default();
                    let url = aliclient
                        .api_send_sms(&template_id, &phone, params.clone())
                        .await
                        .map_err(|_| warp::reject())?;
                    Result::<_, warp::Rejection>::Ok(warp::reply::json(&json!({
                        "code": 200,
                        "url": url
                    })))
                }
                ApiBody::EventDisableRequest { event_kind, state } => {
                    let config = config();
                    config.enable_event(event_kind, state);
                    Result::<_, warp::Rejection>::Ok(warp::reply::json(&json! ({
                        "code": 200
                    })))
                }
            }
        })
        .with(warp::reply::with::header(
            "Content-Type",
            "application/json; charset=utf-8",
        ))
        .boxed();
    let rpc_port = config().rpc_port();
    warp::serve(promote)
        .run(([0u8, 0u8, 0u8, 0u8], rpc_port))
        .await
}
