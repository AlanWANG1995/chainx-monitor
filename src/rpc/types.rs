use crate::watcher::types::EventKind;
use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize)]
#[serde(untagged)]
pub enum ApiBody {
    RpcSendSMSRequest {
        phone: String,
        template_id: String,
        params: serde_json::Value,
    },
    EventDisableRequest {
        event_kind: EventKind,
        state: bool,
    },
}

#[test]
fn test_serialize() {
    let e = serde_json::to_string(&ApiBody::EventDisableRequest {
        event_kind: EventKind::NewBlockTakeTooLong,
        state: true,
    });
    println!("{:?}", e);
}
