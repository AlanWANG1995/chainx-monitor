pub mod service;

use std::sync::{Arc, RwLock};

use futures::channel::mpsc::channel;

use crate::trigger::types::TriggerEvent;
use crate::watcher::Middleware;

pub use crate::watcher::types::Event;

#[derive(Clone)]
pub struct Monitor {
    middlewares: Arc<RwLock<Vec<Middleware>>>,
}

impl Monitor {
    pub fn new() -> Self {
        Self {
            middlewares: Arc::new(RwLock::new(vec![])),
        }
    }

    #[allow(irrefutable_let_patterns)]
    pub fn run(&self) {
        // TriggerRegistry -TriggerEvent-> WatcherRegistry
        let (trigger_tx, trigger_rx) = channel::<TriggerEvent>(1024);
        let trigger_registry = crate::trigger::trigger_registry();
        trigger_registry
            .run(trigger_tx)
            .expect("Trigger Starts failed.");

        // WatcherRegistry -Event-> ActorRegistry
        let (watcher_tx, watcher_rx) = channel::<Event>(1024);
        let watcher_registry = crate::watcher::watcher_registry();
        watcher_registry.async_run(trigger_rx, watcher_tx);

        let actor_registry = crate::actor::sequence_actor_registry();
        actor_registry.async_run(watcher_rx);
    }
}
