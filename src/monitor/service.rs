use anyhow::Result;
use hmacsha1::hmac_sha1;
use percent_encoding::{utf8_percent_encode, AsciiSet, NON_ALPHANUMERIC};
use serde_json::json;

macro_rules! map {
    (($end_x: expr, $end_y: expr) $(,($x:expr, $y:expr))* ) => {
        {
            let mut bmap = std::collections::BTreeMap::new();
            bmap.insert($end_x.to_string(), $end_y.to_string());
            $(bmap.insert($x.to_string(), $y.to_string());)*
            bmap
        }
    };
}

fn special_url_encode(s: String) -> String {
    const FILTER_SET: &AsciiSet = &NON_ALPHANUMERIC.remove(b'-').remove(b'_').remove(b'.');
    let result = utf8_percent_encode(s.as_str(), FILTER_SET).to_string();
    result
        .replace('+', "%20")
        .replace('*', "%2A")
        .replace("%7E", "~")
}

fn sign(s: &str, key: &[u8]) -> String {
    let data = hmac_sha1(key, s.as_bytes());
    base64::encode(data)
}

#[derive(Clone)]
pub struct AliServiceClient {
    signer: String,
    access_key_id: String,
    access_secret: String,
}

impl Default for AliServiceClient {
    fn default() -> Self {
        Self {
            signer: "链网科技".to_string(),
            access_key_id: "LTAI4G4AZHrWtdYk24xL6WXB".to_string(),
            access_secret: "eQF1729Nz1wc1PbF7WxvUID4L3FQTX".to_string(),
        }
    }
}

const PHONE_NUMBERS: &str =
    "17612712021,15019487202,15658169307,18510727491,15168355842,15827489751,15779385439";

impl AliServiceClient {
    #[allow(dead_code)]
    pub fn from_env() -> Self {
        Self {
            signer: std::env::var("SIGNER").expect("SIGNER environment variable missing"),
            access_key_id: std::env::var("ACCESS_KEY_ID")
                .expect("ACCESS_KEY_ID environment variable missing"),
            access_secret: std::env::var("ACCESS_KEY_SECRET")
                .expect("ACCESS_KEY_SECRET environment variable missing"),
        }
    }

    pub async fn api_send_sms(
        &self,
        template_id: &str,
        phone: &str,
        param_json: serde_json::Value,
    ) -> Result<String> {
        let params = map! {
            ("SignatureMethod", "HMAC-SHA1"),
            ("SignatureNonce", uuid::Uuid::new_v4().to_hyphenated()),
            ("AccessKeyId", self.access_key_id),
            ("SignatureVersion", "1.0"),
            ("Timestamp", humantime::format_rfc3339(std::time::SystemTime::now())),
            ("Format", "JSON"),
            ("Action", "SendSms"),
            ("Version", "2017-05-25"),
            ("RegionId", "cn-hangzhou"),
            ("PhoneNumbers", phone),
            ("SignName", self.signer),
            ("TemplateParam", param_json),
            ("TemplateCode", template_id)
        };

        let mut url = String::default();
        for (key, val) in params.iter() {
            url.push_str(&format!("&{}={}", key, special_url_encode(val.clone())));
        }
        let query_str = url[1..].to_string();

        let string_to_sign = format!(
            "GET&{}&{}",
            special_url_encode("/".to_string()),
            special_url_encode(query_str.clone())
        );

        let sign = sign(
            &string_to_sign,
            format!("{}&", self.access_secret).as_bytes(),
        );
        let signature = special_url_encode(sign.clone());
        let url = format!(
            "http://dysmsapi.aliyuncs.com/?Signature={}&{}",
            signature, query_str
        );
        log::debug!("{}", url);
        let response = reqwest::get(&url).await.unwrap().text().await.unwrap();

        Ok(format!(
            r#"
                {{
                    "status": 20000,
                    "msg": {}
                }}
                "#,
            response
        ))
    }

    pub async fn short_warn(&self, msg: &str) -> Result<String> {
        let template_id = "SMS_205464493";
        let params = json!({
            "msg": msg.to_string()
        });
        self.api_send_sms(template_id, PHONE_NUMBERS, params).await
    }

    pub async fn send_block_timeout(&self, msg: &u32) -> Result<String> {
        let template_id = "SMS_205876729";
        let params = json!({
            "height": msg.to_string()
        });
        self.api_send_sms(template_id, PHONE_NUMBERS, params).await
    }
}

pub fn async_notify(message: String) {
    tokio::task::spawn(async move {
        if let Err(e) = AliServiceClient::default().short_warn(&message).await {
            log::error!("Error when sending sms: {:?}", e);
        }
    });
}

pub fn async_send_block_timeout(message: u32) {
    tokio::task::spawn(async move {
        if let Err(e) = AliServiceClient::default()
            .send_block_timeout(&message)
            .await
        {
            log::error!("Error when sending block timeout: {:?}", e);
        }
    });
}
