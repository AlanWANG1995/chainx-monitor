import React, {MouseEventHandler, useState} from 'react';
import {makeStyles} from '@material-ui/core/styles';
import {Card, CardContent, CardActions, Switch, Button, Typography} from '@material-ui/core';

const useEventStatus = () => ({
  "RpcTimeout": useState(true),
  "ValidatorOffline": useState(true),
  "ExplorerDown": useState(true),
  "BTCRelayDown": useState(true),
  "NewBlockTakeTooLong": useState(true),
  "TooManyUnfinalizedBlocks": useState(true),
  "IrregularSelfBondedUnlock": useState(true),
  "WrongTotalIssuance": useState(true),
  "WrongTotalBtcIssuance": useState(true),
  "WrongTotalNomination": useState(true),
  "WrongTotalUnlockingBalances": useState(true),
});

const useStyles = makeStyles((theme) => ({
  seeMore: {
    marginTop: theme.spacing(3),
  },
}));

export default function Orders() {
  const classes = useStyles();
  const eventStatus = useEventStatus()
  return (
    <div
      style={{
        display: 'flex',
        flexWrap: 'wrap'
      }}
    >
      {
        Object.entries(eventStatus).map(([k, [status, setStatus]]) => (
          <Card
            style={{
              margin: '0.5vh'
            }}
          >
            <CardContent>
              <Typography>
                {k}
              </Typography>
            </CardContent>
            <CardActions
              style={{
                display: 'flex',
                justifyContent: 'space-between'
              }}
            >
              <Switch
                checked={status}
                onChange={async (_, v) => {
                  const reponse = await fetch("/api/v1", {
                    method: "POST",
                    cache: "no-cache",
                    headers: {
                      'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                      "event_kind": k,
                      "state": v
                    })
                  });
                  if (reponse.status === 200) {
                    setStatus(v)
                  } else {
                    alert("Remote rpc error.");
                  }
                }}
              />
              <Button
                color="primary"
              >
                <Typography>
                  Log
                </Typography>
              </Button>
            </CardActions>
          </Card>
        ))
      }
    </div>
  );
}

