# Chainx Monitor

监控指标：

- [x] 投票用户的所有待解锁之和 `unbondedChunks` 等于锁定中的待解锁金额 `LockedType::BondedWithdrawal`。
- [x] 节点总得票等于该节点下面的所有投票用户对该节点的投票之和。
- [x] 节点最新总票龄等于该节点的所有投票用户的最新总票龄之和。
- [x] 全链所有用户 PCX 总余额等于 PCX 总发行量。
- [x] 全链所有用户 XBTC 总余额等于 XBTC 总发行量。
- [ ] 投票用户的 Staking 锁定总额等于用户余额中的 `staking ` lock 的资金。
- [x] 浏览器 API 可用性
  检查10个浏览器的api，若可用api低于4个，则报警。
- [x] BTC Relay 可用性
  每小时检查获取的区块高度， 并和"https://api.blockchair.com/bitcoin/stats"所获取到的比较， 当落后6个块时报警。
- [x] 验证人离线 `imOnline.SomeOffline`.
- [x] 长时间未收到块
  超过60秒未收到新的块， 则报警
- [x] 节点 RPC 服务无响应 Cannot connect to any built-in or configured node.
- [x] 长时间未确认块
  超过30个块未确认
- [x] 监控钱包节点
  + 117.51.148.252
  + 117.51.150.77
  + 117.51.142.70
  + 117.51.151.223
- [x] 监控自检
  当有watcher停止运行时，通过钉钉告警
- [x] 远程控制
  通过浏览器暂时停止特定事件的报警
  

架构：
分为三个组件：`Trigger`, `Watcher`和`Actor`。 每个组件有接受输入\出的单一`Receiver\Sender`， 并在内部决定事件的分发。
目前实现中， `Watcher`的事件分发是并行的
```
             Input
             / | \
            /  |  \
           A   B   C
           \   |   /
            \  |  /
            Output
```
而`Actor`的事件分发是串行的。

配置：
- `--node-url`: 指定链接节点的ws地址  
- `--debug`: debug模式下将不会发送短信  
